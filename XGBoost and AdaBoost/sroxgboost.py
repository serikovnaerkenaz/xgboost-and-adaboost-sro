import pandas as pd
import xgboost as xgb
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder

# CSV файлынан деректерді жүктейміз
data = pd.read_csv('C:/Users/Yerkenaz/Desktop/сро моис/airports.csv')

# Категориялық деректерді Label Encoding көмегімен түрлендіреміз
label_encoder = LabelEncoder()
for col in ['id','ident','type','name','latitude_deg','longitude_deg','elevation_ft','continent','iso_country','iso_region','municipality','scheduled_service','gps_code','iata_code','local_code','home_link','wikipedia_link','keywords','score','last_updated']:
    data[col] = label_encoder.fit_transform(data[col])

X = data.drop(columns=['name'])
y = data['name']

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

# Категориялық деректерді ескере отырып матрица жасаймыз
dtrain = xgb.DMatrix(data=X_train, label=y_train, enable_categorical=True)
dtest = xgb.DMatrix(data=X_test, label=y_test, enable_categorical=True)

# Xgboost моделінің параметрлерін енгіземіз
params = {
    'objective': 'reg:squarederror',  # Регрессия үшін жоғалту функциясы
    'eval_metric': 'rmse'  # Модельдің сапасын бағалау көрсеткіші
}

# XGBoost моделін оқытамыз
num_rounds = 20 # Итерация саны
model = xgb.train(params=params, dtrain=dtrain, num_boost_round=num_rounds, evals=[(dtest, 'eval')])

predictions = model.predict(dtest)

# (RMSE) Орташа квадраттық қателікпен модельдің сапасын бағалау
from sklearn.metrics import mean_squared_error
rmse = mean_squared_error(y_test, predictions, squared=False)
rounded_rmse = round(rmse, 2)
formatted_rmse = f'{rounded_rmse}%'
print(f'Орташа квадраттық қателік: {formatted_rmse}')