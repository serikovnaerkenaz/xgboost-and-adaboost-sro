import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.ensemble import AdaBoostRegressor
from sklearn.metrics import mean_squared_error
from sklearn.preprocessing import LabelEncoder

#  CSV файлынан керекті бағандардағы дерекктерді аламыз
data = pd.read_csv('C:/Users/Yerkenaz/Desktop/сро моис/airports.csv', usecols=['latitude_deg', 'longitude_deg', 'score'])

data['score'] = pd.to_numeric(data['score'], errors='coerce')

# 'Score' бағанында жоқ мәндері бар жолдарды жойып тастау керек
data = data.dropna(subset=['score'])

X = data[['latitude_deg', 'score']]
y = data['longitude_deg']

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

# AdaBoostRegressor моделін құрамыз
model = AdaBoostRegressor(n_estimators=50, random_state=42)

# Моделді оқыту
model.fit(X_train, y_train)

predictions = model.predict(X_test)

# (RMSE) Орташа квадраттық қателікпен модельдің сапасын бағалау
rmse = mean_squared_error(y_test, predictions, squared=False)
rounded_rmse = round(rmse, 2)
print(f'Орташа квадраттық қателік: {rounded_rmse}%')